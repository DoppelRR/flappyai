"""
2-input XOR example -- this is most likely the simplest possible example.
"""

from __future__ import print_function
import os
import neat
from neat import Checkpointer
import visualize
from flappy import Flappy
from datetime import datetime
import numpy as np
import time

# 2-input XOR inputs and expected outputs.

def eval_genomes(genomes, config):
    seed = datetime.now()
    bestgenome = None
    bestscore = 0
    for genome_id, genome in genomes:
        net = neat.nn.FeedForwardNetwork.create(genome, config)

        steps = 0
        f = Flappy(seed=seed)
        running = True
        while running:
            output = net.activate([f.birdY, f.pipeY])
            jump = output.index(max(output))
            steps += 1
            running = f.nextStep(jump == 1)
            if steps == 3000:
                break
        genome.fitness = steps
        if steps > bestscore:
            bestscore = steps
            bestgenome = genome
    showGame(seed, bestgenome, config)

def showGame(seed, genome, config):
    f = Flappy(seed)
    net = neat.nn.FeedForwardNetwork.create(genome, config)
    running = True
    while running:
        output = net.activate([f.birdY, f.pipeY])
        jump = output.index(max(output))
        running = f.nextStep(jump == 1)
        print(str(running) + str(jump))
        time.sleep(0.03)


def run(config_file):
    # Load configuration.
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_file)

    # Create the population, which is the top-level object for a NEAT run.
    p = neat.Population(config)
    #p = Checkpointer.restore_checkpoint("neat-checkpoint-9")

    # Add a stdout reporter to show progress in the terminal.
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)
    p.add_reporter(neat.Checkpointer(5))

    # Run for up to 300 generations.
    winner = p.run(eval_genomes)

    # Display the winning genome.
    print('\nBest genome:\n{!s}'.format(winner))

    # Show output of the most fit genome against training data.
    print('\nOutput:')
    winner_net = neat.nn.FeedForwardNetwork.create(winner, config)

    node_names = {-1:'A', -2: 'B', 0:'A XOR B'}
    visualize.draw_net(config, winner, True, node_names=node_names)
    visualize.plot_stats(stats, ylog=False, view=True)
    visualize.plot_species(stats, view=True)

    p = neat.Checkpointer.restore_checkpoint('neat-checkpoint-4')
    p.run(eval_genomes, 10)


if __name__ == '__main__':
    # Determine path to configuration file. This path manipulation is
    # here so that the script will run successfully regardless of the
    # current working directory.
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'config-feedforward')
    run(config_path)