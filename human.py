import pygame
from flappy import Flappy
import time


f = Flappy()

running = True
while running:
    jump = False
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            jump = True
        if event.type == pygame.QUIT:
            running = False       

    if not running:
        break

    running = f.nextStep(jump)
    time.sleep(0.1)

print("Score: " + str(f.score))
