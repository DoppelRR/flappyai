import pygame
import random
from datetime import datetime

class Flappy:

    BOARD_WIDTH = 256
    BOARD_HEIGHT = 256
    TEXTURE_SIZE = 16

    birdY = 60

    pipeY = 10
    pipeX = BOARD_WIDTH
    screen = None
    score = 0

    end = 0

    def __init__(self, seed=None, end=0):
        if seed is None:
            random.seed(datetime.now())
        else:
            random.seed(seed)
        self.end = end
        pygame.init()
        pygame.display.set_caption("Flappy")
        self.screen = pygame.display.set_mode((self.BOARD_WIDTH, self.BOARD_HEIGHT))

        self.birdImage = pygame.image.load("Bird.png")
        self.pipeImage = pygame.image.load("Pipe.png")

        self.pipeY = random.randint(3, 10)

    def nextStep(self, jump):
        if jump:
            self.birdY -= 15
        else:
            self.birdY += 5

        self.pipeX -= 3
        if self.pipeX < 0:
            self.pipeY = random.randint(3, 10)
            self.pipeX = self.BOARD_WIDTH
            self.score += 1

        if self.score == 15:
            return False

        if self.pipeX <= 26 and self.pipeX >= 10 and (self.birdY <= self.pipeY * self.TEXTURE_SIZE or self.birdY >= (self.pipeY + 3) * self.TEXTURE_SIZE):
            return False

        if self.birdY >= self.BOARD_HEIGHT or self.birdY < 0:
            return False

        # Drawing
        self.screen.fill((0, 0, 0))

        self.screen.blit(self.birdImage, (10, self.birdY))
        for y in range(self.pipeY):
            self.screen.blit(self.pipeImage, (self.pipeX, y * self.TEXTURE_SIZE))

        for y in range(self.pipeY + 3, 16):
            self.screen.blit(self.pipeImage, (self.pipeX, y * self.TEXTURE_SIZE))

        pygame.display.flip()
        return True

